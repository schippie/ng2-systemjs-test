var template = require('lodash.template');
var process = require('process');
var fs = require('fs');

var modus = "DEV";
if(process.argv[2] != null && process.argv[2].length > 0) modus = process.argv[2];

fs.readFile('src/index.html.tpl', function(error, data) {
    if(error) throw error;
    
    var tpl = template(data);
    var compiledTpl = tpl({modus: modus});
    
    if(modus !== 'DEV') {
        fs.writeFile('dist/index.html', compiledTpl, function(error) {
            if(error) console.log(error);
            console.log('file /dist/index.html was created and saved');
        });
    }
    
    fs.writeFile('index.html', compiledTpl, function(error) {
        if(error) console.log(error);
        console.log('file /index.html was created and saved');
    });
});
