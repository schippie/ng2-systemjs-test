var path = require("path");
var Builder = require('systemjs-builder');
var minifier = require('minifier');

/**
 * SystemJS builder configuration
 */
var builder = new Builder('/', './system.config.js');
builder
.bundle('src/main.js', 'dist/bundle.js', {
    minify: true,
    sourceMaps: true
})
.then(function() {
  console.log('Build complete');
})
.catch(function(err) {
  console.log('Build error');
  console.log(err);
});

/**
 * Polyfill minifier JS files
 */
minifier.on('error', function(error) {
    console.error(error);
});
minifier.minify([
     "node_modules/systemjs/dist/system.src.js",
     "node_modules/systemjs/dist/system-polyfills.js",
     "node_modules/reflect-metadata/Reflect.js",
     "node_modules/zone.js/dist/zone.js",
     "node_modules/angular2/es6/dev/src/testing/shims_for_IE.js",
     "system.config.js"
], {
    output: 'dist/polyfill.js'
})

/**
 * Minify css file
 */
minifier.minify('tmp/style.css', {output: 'dist/style.css'});