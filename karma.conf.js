/**
 * Karma configuration to run the specified unit tests within the test folder.
 * All files with the name <<filename>>.spec.js will be run by this test runner.
 */
var path = require('path');
module.exports = function (config) {
    config.set({
        basePath: '',
        // Start these browsers, currently available:
        // - Chrome
        // - Firefox
        // - PhantomJS
        browsers: ['Chrome'],
        frameworks: ['jasmine'],
        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,
        autoWatch: false,
        singleRun: true,
        port: 9876,
        files: [
            //Base requirements
            {pattern: 'node_modules/systemjs/dist/system.src.js', included: true, watched: true},
            {pattern: 'node_modules/systemjs/dist/system-polyfills.js', included: true, watched: true},
            {pattern: 'node_modules/angular2/bundles/angular2-polyfills.js', included: true, watched: true},
            {pattern: 'node_modules/angular2/es6/dev/src/testing/shims_for_IE.js', included: true, watched: true},
            {pattern: 'node_modules/angular2/bundles/testing.dev.js', included: true, watched: true},
            
            //Packages /modules recursively included
            {pattern: 'node_modules/angular2/**/*.js', included: false, watched: false},
            {pattern: 'node_modules/rxjs/**/*.js', included: false, watched: false},
            {pattern: 'node_modules/systemjs-plugin-text/**/*.js', included: false, watched: false},

            //Our own files
            {pattern: 'test/test.system.config.js', watched: true, included: true},
            {pattern: 'src/**/*.ts', included: false, watched: true},
            {pattern: 'src/**/*.html', included: false, watched: true},
            {pattern: 'tmp/**/*.js', included: false, watched: true},
            {pattern: 'tmp/**/*.js.map', included: false, watched: true},
            {pattern: 'test/**/*.ts', included: false, watched: true},

            //Import unit test files
            {pattern: 'test/spec.import.js', included: true, watched: true}
        ],
        preprocessors: {
            'src/**/*.html': ['redirect']
        },
        customLaunchers: {
            Chrome_with_debugging: {
                base: 'Chrome',
                chromeDataDir: path.resolve(__dirname, '.chrome')
            }
        }
    });
};