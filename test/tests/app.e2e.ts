describe('AppComponent', () => {
	beforeEach(() => {
		browser.get('/');
	});

	it('should have a title', function () {
		var subject = browser.getTitle();
		var result = 'N. Schipper SystemJS';
		expect(subject).toEqual(result);
	});
});