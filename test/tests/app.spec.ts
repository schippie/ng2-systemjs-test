import {AppComponent} from "../../src/app";
import {Subject} from "rxjs/Subject";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";

describe('App spec test', () => {
    it('should assert that true equals true', () => {
        expect(true).toBe(true);
    });

    it('should assert that the app.name is equal to Nick Schipper 3', () => {
        let app = new AppComponent();
        expect(app.name).toBe('Nick Schipper 3');
    });
    
    it('should not complete', () => {
        let subject = new Observable((subscription) => {
            subscription.next(1);
            subscription.next(2);
            subscription.next(3);
            subscription.next(4);
            subscription.next(5);
        });
        let observable = subject.map((acc, data) => {
            acc.push(data);
            return acc;
        }, []);
        
        
        let subscription = observable.subscribe((value) => {
            console.log(value);
        });
    })
    
});