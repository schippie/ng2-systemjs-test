// Tun on full stack traces in errors to help debugging
Error.stackTraceLimit = Infinity;
jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;
__karma__.loaded = function() {};

System.import('angular2/src/platform/browser/browser_adapter')
    .then(function(browser_adapter) { browser_adapter.BrowserDomAdapter.makeCurrent(); })
    .then(function() { return Promise.all(resolveTestFiles()); })
    .then(function() { __karma__.start(); }, function(error) { console.error(error.stack || error); });

function resolveTestFiles() {
    return Object.keys(window.__karma__.files)  // All files served by Karma.
        .filter(function(path) { return /\.spec\.ts$/.test(path); })
        .map(function(moduleName) {
            var names = moduleName.match(/\/base\/(.*).ts/);
            if(names.length < 2) return;
            console.log('Including spec file: ' + names[1]);
            return System.import(names[1]);
        });
}