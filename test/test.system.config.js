System.config({
    defaultJSExtensions: true,
    baseURL: '/base',
    paths: {
        'src/*': 'tmp/src/*',
        'src/*.html': 'src/*.html',
        'test/*': 'tmp/test/*',
        '*': 'node_modules/*',
        'es6-shim': 'node_modules/es6-shim/es6-shim.min.js'
    },
    packageConfigPaths: ['node_modules/*/package.json'],
    map: {
        text: 'systemjs-plugin-text/text.js'
    }
});