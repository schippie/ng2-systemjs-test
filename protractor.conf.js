/*jshint node: true*/
/*global exports: true*/

/**
 * Protractor tests configurations. This causes all *.e2e.ts test files to be run
 * This validates all the end to end tests that have been written.
 **/
var packageJsonCnf = require('./package.json');
exports.config = {
	baseUrl: packageJsonCnf._baseUrl + ':' + packageJsonCnf["local-web-server"].port + '/',
	allScriptsTimeout: 110000,
	seleniumServerJar: "node_modules/protractor/selenium/selenium-server-standalone-2.52.0.jar",
	capabilities: {
		'browserName': 'chrome',
		'chromeOptions': {
			'args': ['show-fps-counter=true']
		}
	},
	framework: 'jasmine2',
	specs: [
		'tmp/test/**/**.e2e.js',
		'tmp/test/**/*.e2e.js'
	],
    exclude: [],
    directConnect: true,
	jasmineNodeOpts: {
        showTiming: true,
        showColors: true,
        isVerbose: false,
        includeStackTrace: false,
        defaultTimeoutInterval: 400000
	},
	useAllAngular2AppRoots: true
};