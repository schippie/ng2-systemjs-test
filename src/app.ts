import {Component, OnDestroy, OnInit} from 'angular2/core';
import {Title} from "angular2/src/platform/browser/title";
import template from "src/app.html!text"; 

@Component({
    selector: 'my-app',
    template: template //'<h1>{{name}} First Angular 2 App</h1>'
})
export class AppComponent implements OnDestroy, OnInit {
    name: string;
    private title: Title = new Title();

    constructor() {
        this.title.setTitle("N. Schipper SystemJS");
        this.name = 'Nick Schipper 3';
    }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }
}