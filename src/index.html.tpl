<html>
  <head>
    <title>Angular 2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <base href="/">
  </head>

  <!-- 4. Display the application -->
  <body>
    <my-app>Loading...</my-app>
    
    <!-- DEV CONFIGURATION -->
    <% if(modus === 'DEV') { %>
        <!-- 1. Load CSS -->
        <link rel="stylesheet" href="tmp/style.css">    
        
        <!-- 2. Load libraries -->
        <!-- IE required polyfills, in this exact order -->
        <script src="node_modules/systemjs/dist/system.src.js"></script>
        <script src="node_modules/systemjs/dist/system-polyfills.js"></script>
        
        <!-- 3. Load angular libraries -->
        <!--<script src="node_modules/angular2/bundles/angular2-polyfills.js"></script>-->
        <script src="node_modules/reflect-metadata/Reflect.js"></script>
        <script src="node_modules/zone.js/dist/zone.js"></script>
        <script src="node_modules/angular2/es6/dev/src/testing/shims_for_IE.js"></script>
        
        <!-- 4. Configure SystemJS -->
        <script type="text/javascript" src="system.config.js"></script>
    <% } %>
    
    <!-- DEV CONFIGURATION WITH LIVERELOAD-->
    <% if(modus === 'DEV-LIVERELOAD') { %>
        <!-- 1. Load CSS -->
        <link rel="stylesheet" href="tmp/style.css">    
        
        <!-- 2. Load libraries -->
        <!-- IE required polyfills, in this exact order -->
        <script src="node_modules/systemjs/dist/system.src.js"></script>
        <script src="node_modules/systemjs/dist/system-polyfills.js"></script>
        
        <!-- 3. Load angular libraries -->
        <!--<script src="node_modules/angular2/bundles/angular2-polyfills.js"></script>-->
        <script src="node_modules/reflect-metadata/Reflect.js"></script>
        <script src="node_modules/zone.js/dist/zone.js"></script>
        <script src="node_modules/angular2/es6/dev/src/testing/shims_for_IE.js"></script>

        <!-- 4. Live reload server configuration-->
        <script>
        document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>');
        </script>
        
        <!-- 5. Configure SystemJS -->
        <script type="text/javascript" src="system.config.js"></script>
    <% } %>
    
    <!-- STAGING CONFIGURATION -->
    <% if(modus === 'STAGING') { %>
        <!-- 1. Load CSS -->
        <link rel="stylesheet" href="dist/style.css">
        <!-- 2. Load libraries -->
        <script type="text/javascript" src="dist/polyfill.js"></script>
        <!-- 3. Load our project -->
        <script type="text/javascript" src="dist/bundle.js"></script>
    <% } %> 
    
    <!-- PRODUCTION CONFIGURATION -->
    <% if(modus === 'PRODUCTION') { %> 
        <!-- 1. Load CSS -->
        <link rel="stylesheet" href="dist/style.css">
        <!-- 2. Load libraries -->
        <script type="text/javascript" src="dist/polyfill.js"></script>
        <!-- 3. Load our project -->
        <script type="text/javascript" src="dist/bundle.js"></script>
    <% } %> 
    
    <script>
      System.import('src/main.js').then(undefined, console.error.bind(console));
    </script>
  </body>
</html>