////<reference path="./typings/typings.d.ts" />

//import {provide, enableProdMode} from 'angular2/core';
import 'es6-shim';
import {bootstrap, ELEMENT_PROBE_PROVIDERS} from 'angular2/platform/browser';
import {AppComponent} from './app'

//set the environment type
const ENV_PROVIDERS = [];
ENV_PROVIDERS.push(ELEMENT_PROBE_PROVIDERS);

/**
 * We bootstrap or application here. We only add our CUSTOM PROVIDERS into the bootstrap to keep this class nice and clean
 * If we require other providers please add those to the array of providers within ./injectables/providers/ and not here!
 */
bootstrap(AppComponent, [ENV_PROVIDERS])
    .catch(error => console.error(error));